/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tubes.ai;

import java.util.Arrays;

/**
 *
 * @author Prana
 */
public class Community {
    Population p[] = new Population[Population.amount];
        
    public Community() {
        int k = 0;
        while(k < Population.amount){
            p[k] = new Population();
            k++;
        }
//        p[0] = new Population();
        Individual[] candidate = new Individual[2];
        for (int i = 1; i < Population.amount; i++) {
            
            p[i].individual[0] = p[i-1].elitismIndividual;
            
            int j = 1;
//            Population.getAllIndividual(p, i-1);
            // 1 12 , 2 23
            // 1 12 , 3 34 , 5
            while(j < 5){
                candidate = Population.bestParent(p, i-1);
//                System.out.println(Arrays.toString(candidate[0].chromosome.chromosome));                
//                System.out.println(Arrays.toString(candidate[1].chromosome.chromosome));

                p[i].individual[j] = new Individual(Crossover.order1Crossover(candidate[0].chromosome, candidate[1].chromosome));
                p[i].individual[j+1] = new Individual(Crossover.order1Crossover(candidate[1].chromosome, candidate[0].chromosome));
                j = j+1;
            }
            p[i].elitismIndividual = p[i].biggestFitness(p[i]);
//            System.out.println("Selesai 1 populasi");
//            Population.getAllIndividual(p, i);
        }
    }
    
}
