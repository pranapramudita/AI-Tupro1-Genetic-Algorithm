/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tubes.ai;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 *
 * @author Prana
 */
public class Population {
    static int amount = 200;
    Individual[] individual = new Individual[Individual.amount];
    static Individual bestParent[] = new Individual[2];
    Individual elitismIndividual = new Individual();

    public Population() {
        int i = 0;
        while(i < Individual.amount){
            individual[i] = new Individual();
            i++;
        }
        elitismIndividual = biggestFitness(this);
    }
    
    static void getAllPopulation(Population[] p){
        for (int i = 0; i != Population.amount; i++) {
            System.out.println("Populasi "+(i+1));
            for (int j = 0; j != Individual.amount; j++) {
                
                System.out.print("Individu "+(j+1)+" : "+Arrays.toString(p[i].individual[j].chromosome.chromosome));
                System.out.printf(" Fitness = "+"%.29f",p[i].individual[j].chromosome.fitness);
                System.out.println();
                
            }System.out.println();
        }
    }
    
    static void getAllIndividual(Population[] p, int i){
        for (int j = 0; j < Individual.amount; j++) {
            System.out.print("Individu "+(j+1)+" : "+Arrays.toString(p[i].individual[j].chromosome.chromosome));
            System.out.printf(" Fitness = "+"%.29f",p[i].individual[j].chromosome.fitness);
            System.out.println();
        }System.out.println();
    }
    
    static Individual[] bestParent(Population[] p, int i){
        double x = 0;
        Individual[] candidateParent = new Individual[2];
        List<Individual> temp = new ArrayList<>();
        Population[] newTemp = new Population[2];
        Population pa = new Population();
        Random random = new Random();
        int l = 0;
        int a = 0;
        while(candidateParent[0] == candidateParent[1]){
            a = 0;
            while(a < 2){
                temp = new ArrayList<>();
                for (int j = 0; j < 5; j++) {
                    l = random.nextInt(Individual.amount);
                    if(!temp.contains(p[i].individual[l])){
                        temp.add(p[i].individual[l]);
                    }
                }
//                for (Individual col : temp) {
//                    System.out.print(Arrays.toString(col.chromosome.chromosome));
//                    System.out.printf(" fitness : "+"%.29f",col.chromosome.fitness);
//                    System.out.println();
//                }
//                System.out.println("-----------");
                pa = new Population();
                pa.individual = temp.toArray(new Individual[Individual.amount]);
                newTemp[a] = pa;
                a++;
            }

            candidateParent[0] = biggestFitness(newTemp[0],5);  
            candidateParent[1] = biggestFitness(newTemp[1],5);
        }
        
//        for (int k = 0; k < bestParent.length; k++) {
//            x = 0;
//            if(k == 0){
//                    a = 0;
//                    l = Individual.amount/2;
//            }else if(k == 1){
//                    a = Individual.amount/2;
//                    l = Individual.amount;
//            }
//            for (int j = a; j < l; j++) {
//                if(p[i].individual[j].chromosome.fitness > x){
//                    x = p[i].individual[j].chromosome.fitness;
//                    candidateParent[k] = p[i].individual[j];
//                }
//            }
//        }
        return candidateParent;
    }    
    
    static Individual biggestFitness(Population p){
        Individual candidateParent = new Individual();
        double x = 0;
        for (int j = 0; j < p.individual.length; j++) {
            if(p.individual[j].chromosome.fitness > x){
                x = p.individual[j].chromosome.fitness;
                candidateParent = p.individual[j];
            }
        }return candidateParent;
    }
    
    static Individual biggestFitness(Population p, int i){
        Individual candidateParent = new Individual();
        double x = 0;
        for (int j = 0; j < i; j++) {
            if(p.individual[j] != null){
                if(p.individual[j].chromosome.fitness > x){
                    x = p.individual[j].chromosome.fitness;
                    candidateParent = p.individual[j];
                }
            }
            
        }return candidateParent;
    }
}
