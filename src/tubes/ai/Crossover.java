/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tubes.ai;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

/**
 *
 * @author Prana
 */
public class Crossover {
    static void singleCrossover(Chromosome p1, Chromosome p2){
        Random ran = new Random();
        int i = ran.nextInt(10);
        int temp = p1.chromosome[i];
        p1.chromosome[i] = p2.chromosome[i];
        p2.chromosome[i] = temp;
        
        System.out.println("random k = "+i);
        System.out.println(Arrays.toString(p1.chromosome));
        System.out.println(Arrays.toString(p2.chromosome));
    }
    
    static void simpleArithmeticCrossover(Chromosome p1, Chromosome p2){
        Random ran = new Random();
        int i = ran.nextInt(10);
        int j = i;
        while(j != p1.chromosome.length){
            int temp = p1.chromosome[j];
            p1.chromosome[j] = p2.chromosome[j];
            p2.chromosome[j] = temp;
            j++;
        }
        
        System.out.println("random k = "+i);
        System.out.println(Arrays.toString(p1.chromosome));
        System.out.println(Arrays.toString(p2.chromosome));
    }
    
    static void wholeArithmeticCrossover(Chromosome p1, Chromosome p2){
        int[] temp = new int[10];
        temp = p1.chromosome.clone();
        p1.chromosome = p2.chromosome;
        p2.chromosome = temp;
        
        System.out.println("Whole Arithmetic Crossover : ");
        System.out.println(Arrays.toString(p1.chromosome));
        System.out.println(Arrays.toString(p2.chromosome));
    }
    
    static Individual order1Crossover(Chromosome p1, Chromosome p2){
        Individual individual = new Individual();
        int[] result = {-1,-1,-1,-1,-1,-1,-1,-1,-1,-1};
        
        Random ran = new Random();
        int min = ran.nextInt(10);
        int max = 0;
        while(max < min){
            max = ran.nextInt(10);
        }
        
        int i = min;
        while(i != max+1){
            result[i] = p1.chromosome[i];
            i++;
        }
//        System.out.println(Arrays.toString(result));
        
        
        ArrayList<Integer> arr = new ArrayList<Integer>(10);
        
        if(min > 0){
            //RUMIT 
            if(max == 9){
                i = 0;
                while(i < p2.chromosome.length){
                    if(!additional.isExist(result, p2.chromosome[i])){
                        arr.add(p2.chromosome[i]);
//                        System.out.println(p2.chromosome[i]);
                    }
                    i++;
                }
            }else{
                i = max+1;
                while(i < p2.chromosome.length){
                    if(!additional.isExist(result, p2.chromosome[i])){
                        arr.add(p2.chromosome[i]);
                    }
                    i++;
                }
                i = 0;
                while(i != max+1){
                    if(!additional.isExist(result, p2.chromosome[i])){
                        arr.add(p2.chromosome[i]);
                    }i++;
                }
            }




            //Masukin arr ke result      
            i = max+1;
            while(i < p2.chromosome.length){
                if(!arr.isEmpty()){
                    result[i] = arr.get(0);
                    arr.remove(0);
                }
                i++;
            }            
            

            i = 0;
            while(i < min){
                if(!arr.isEmpty()){
                    result[i] = arr.get(0);
                    arr.remove(0);
                }i++;
            }
        }else{
            i = min+1;
            while(i < p2.chromosome.length){
                if(!additional.isExist(result, p2.chromosome[i])){
                    arr.add(p2.chromosome[i]);
                }
                i++;
            }
            
            i = 0;
            while(i != max+1){
                if(!additional.isExist(result, p2.chromosome[i])){
                    arr.add(p2.chromosome[i]);
                }
                i++;
            }
//            for (Integer arr1 : arr) {
//                System.out.println(arr1);
//            }
            i = min+1;
            while(i != p2.chromosome.length){
                if(!arr.isEmpty()){
                    result[i] = arr.get(0);
                    arr.remove(0);
                }
                i++;
            }
        }
//        System.out.print("Random area : " + min + "-" + max);
//        System.out.println(" "+Arrays.toString(result));
//        System.out.println();
        individual.chromosome.chromosome = result;
        return individual;
    }
}
