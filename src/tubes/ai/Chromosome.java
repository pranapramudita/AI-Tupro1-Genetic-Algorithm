/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tubes.ai;

import java.util.Arrays;
import java.util.Random;

/**
 *
 * @author Prana
 */
public class Chromosome {
//    int[] chromosome = new int[10];  
    int[] chromosome = {-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,};

    double function;
    double fitness;
    Genotype genotype = new Genotype(3,-3);
    Fenotype fenotype = new Fenotype(2,-2);
    
    public Chromosome() {
        randomGen();
        setXYGenotype();
        setXYFenotype();
        setFunction();
        setFitness();
    }
    
    public Chromosome(Individual i) {
        chromosome = i.chromosome.chromosome;
        setXYGenotype();
        setXYFenotype();
        setFunction();
        setFitness();
    }
    
    void randomGen(){
        int i = 0;
        Random random = new Random();
        
        while(i != chromosome.length){
            int temp = random.nextInt(chromosome.length);
            if(!additional.isExist(chromosome, temp)){
                chromosome[i] = temp;
                i++;
            }
        }
    }
    
    int mergeArray(String xy){
        int max = 0, min = 0;
        if(xy == "x"){
            max = chromosome.length-5;
            min = 0;
        }else if(xy == "y"){
            max = chromosome.length;
            min = 5;
        }
        String temp = null;
        int i = min;
        while(i < max){
            if(i == min){
                temp = String.valueOf(chromosome[i]);
            }else{
                temp = temp + String.valueOf(chromosome[i]);
            }i++;
        }
//        System.out.println(Arrays.toString(chromosome));
//        System.out.println(temp);
        return Integer.parseInt(temp);
    }
    
    void setXYGenotype(){
        genotype.x = mergeArray("x");
        genotype.y = mergeArray("y");
    }
    
    double countFenotype(String xy){
        int x = 0;
        if(xy == "x"){
            x = genotype.x;
        }else if(xy == "y"){
            x = genotype.y;
        }return fenotype.min + (x - genotype.min)/(genotype.max - genotype.min) * (fenotype.max - fenotype.min);
    }
    
    void setXYFenotype(){
        fenotype.x = countFenotype("x");
        fenotype.y = countFenotype("y");
    }
    
    double countFunction(){
        return (4-2.1*Math.pow(fenotype.x,2)+(Math.pow(fenotype.x,4)/3))*Math.pow(fenotype.x,2)+(fenotype.x*fenotype.y)+(-4+4*Math.pow(fenotype.y,2))*Math.pow(fenotype.y,2);
    }
    
    void setFunction(){
        function = countFunction();
    }
    
    double countFitness(){
        return 1/(function+0.001);
    }
    
    void setFitness(){
        fitness = countFitness();
    }
    
    void mutation(){
        Random ran = new Random();
        int random = ran.nextInt(chromosome.length);
        
        System.out.println("Indeks Acak = "+random);
        
        int xa = 9-chromosome[random];
        chromosome[random] = xa;
    }
}
