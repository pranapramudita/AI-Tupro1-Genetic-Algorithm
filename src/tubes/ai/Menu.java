/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tubes.ai;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Prana
 */
public class Menu {

    public Menu() {

        Community c = new Community();
//        Population newGeneration = new Population();
        
        int s = -1;
        int i,j;
        
        while(s != 0){
            
            System.out.println("---------------------------------------------------------");
            System.out.println("Tugas Paralel 1 CCH3F3 Kecerdasan Buatan Ganjil 2019/2020");
            System.out.println("Genetic Algorithm (GA)");
            System.out.println("Nama : Prana Pramudita Kusdiananggalih");
            System.out.println("NIM  : 1301174059");       
            
            System.out.println("Jumlah Populasi : "+Population.amount);
            System.out.println("Jumlah Individu : "+Individual.amount);

            System.out.println("Menu :");
            System.out.println("1. Lihat Seluruh Populasi");
            System.out.println("2. Dekode Kromosom");
//            System.out.println("3. Pergantian Generasi");
            System.out.println("4. Mutasi");
            System.out.println("5. Output Sistem Berdasarkan Permintaan Soal");
            System.out.println("0. Keluar");
            System.out.print("Masukan pilihan : ");

            Scanner in = new Scanner(System.in); 
            s = in.nextInt();

            System.out.println("---------------------------------------------------------");
            
            switch(s){
                case 1 : 
                    System.out.println("Lihat Seluruh Pepulasi : ");
                    Population.getAllPopulation(c.p);
                    break;
                case 2 :
                    System.out.println("Dekode Kromosom : ");
                    i = 0;j = 0;
                    System.out.println("Populasi ke-"+(i+1));System.out.println("Individu ke-"+(j+1));
                    System.out.println("Kromosom : "+Arrays.toString(c.p[i].individual[j].chromosome.chromosome));
                    System.out.printf("Fitness : "+"%.29f",c.p[i].individual[j].chromosome.fitness);
                    System.out.println();
                    System.out.println("Genotype x = "+c.p[i].individual[j].chromosome.genotype.x);
                    System.out.println("Genotype y = "+c.p[i].individual[j].chromosome.genotype.y);
                    System.out.println("Fenotype x = "+c.p[i].individual[j].chromosome.fenotype.x);
                    System.out.println("Fenotype y = "+c.p[i].individual[j].chromosome.fenotype.y);
                    break;
//                case 3 :
//                    System.out.println("Pergantian Generasi : ");
//                    i = 0;
//                    System.out.println("Populasi = "+(i+1));
//                    Population.getAllIndividual(c.p,i);
////                    Population.getAllPopulation(c.p);
//                    System.out.println("Pemilihan Orang Tua (Tournament)");
//                    List<Individual> newGen = new ArrayList<>();
//                    while(i < Population.amount){
//                        System.out.println("Random 1-5 Mencari Parent Dengan Fitness Terbaik : ");
//                        c.p[i].bestParent = c.p[i].bestParent(c.p,i);
//                        System.out.println("Best Parent 1 : "+Arrays.toString(c.p[i].bestParent[0].chromosome.chromosome));
//                        System.out.println("Best Parent 2 : "+Arrays.toString(c.p[i].bestParent[1].chromosome.chromosome));
//                        System.out.println();
//                        System.out.println("Crossover 1 : ");
//                        newGen.add(Crossover.order1Crossover(c.p[i].bestParent[0].chromosome, c.p[i].bestParent[1].chromosome));
//                        System.out.println("Crossover 2 : ");
//                        newGen.add(Crossover.order1Crossover(c.p[i].bestParent[1].chromosome, c.p[i].bestParent[0].chromosome));
//                        i++;
//                    }
//                    
////                    System.out.print("Orang Tua 1 : "+additional.findIndex(c.p, i, c.p[i].bestParent[0]));
////                    System.out.println(" "+Arrays.toString(c.p[i].bestParent[0].chromosome.chromosome));   
////                    System.out.print("Orang Tua 2 : "+additional.findIndex(c.p, i, c.p[i].bestParent[1]));
////                    System.out.println(" "+Arrays.toString(c.p[i].bestParent[1].chromosome.chromosome));
////                    System.out.println();
////                    System.out.println("Crossover");
//
//                    Individual[] newGenIndividual = newGen.toArray(new Individual[Individual.amount]);
//                    
//                    newGeneration.individual = newGenIndividual;
//                    System.out.println();
//                    System.out.println("List Generasi Baru Hasil Crossover: ");
//                    for (Individual col : newGeneration.individual) {
//                        if(col != null){
//                            System.out.println(Arrays.toString(col.chromosome.chromosome));
//                        }
//                    }
//                    break;
                case 4 :
                    System.out.println("Mutasi : ");
                    i = 0;j=0;
                    System.out.println("Populasi ke-"+(i+1));System.out.println("Individu ke-"+(j+1));
                    System.out.println("Sebelum Mutasi : "+Arrays.toString(c.p[i].individual[j].chromosome.chromosome));
                    c.p[i].individual[j].chromosome.mutation();
                    System.out.println("Sesudah Mutasi : "+Arrays.toString(c.p[i].individual[j].chromosome.chromosome));
                    break;
                case 5 :
                    System.out.println("Output Sistem Berdasarkan Permintaan Soal : ");
                    System.out.println("Generasi Baru : ");
//                    for (Individual col : newGeneration.individual) {
//                        if(col != null){
//                            System.out.print(Arrays.toString(col.chromosome.chromosome));
//                            System.out.printf(" Fitness : "+"%.29f",col.chromosome.fitness);
//                            System.out.println();
//                        }
//                    }
                    System.out.println("Kromosom Terbaik / Fitness Terbesar : ");
                    System.out.println("Kromosom : "+Arrays.toString(c.p[Population.amount-1].elitismIndividual.chromosome.chromosome));
                    System.out.printf("Fitness : "+"%.29f",c.p[Population.amount-1].elitismIndividual.chromosome.fitness);
                    System.out.println();
                    System.out.println("Hasil Dekode Kromosom Terbaik : ");
                    System.out.println("Genotype x = "+c.p[Population.amount-1].elitismIndividual.chromosome.genotype.x);
                    System.out.println("Genotype y = "+c.p[Population.amount-1].elitismIndividual.chromosome.genotype.y);
                    System.out.println("Fenotype x = "+c.p[Population.amount-1].elitismIndividual.chromosome.fenotype.x);
                    System.out.println("Fenotype y = "+c.p[Population.amount-1].elitismIndividual.chromosome.fenotype.y);
                    break;
            }
        }
        
        
        
//        Population.getAll(c.p);
//        
//        System.out.println(Arrays.toString(c.p[0].individual[0].chromosome.chromosome));
//        c.p[0].individual[0].chromosome.mutation();
//        System.out.println(Arrays.toString(c.p[0].individual[0].chromosome.chromosome));
    }
}
